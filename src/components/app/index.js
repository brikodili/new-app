import React, { useState, useEffect } from 'react'
import users from '../../data/users.json'
import './style.scss';

const defaultUsersState = users.map((user) => user.id).slice(0, 100);

function filter(filters) {
  let ids = defaultUsersState;

  if (filters.male && filters.female) {
    ids = filterByName(ids, filters.name)
  } else if (filters.male) {
    ids = ids.filter((id) => getUserById(id).gender === 'Male')
  } else if (filters.female) {
    ids = ids.filter((id) => getUserById(id).gender === 'Female')
  }

  return filterByName(ids, filters.name)
}

function filterByName(list, name) {
  if (name !== '') {
    return list.filter((id) => getUserById(id).name.includes(name))
  } else {
    return list
  }
}

function getUserById(id) {
  return users.find((user) => user.id === id)
}

export default function() {
  const [selectedUserIds, setSelectedUsers] = useState([]);
  const [allUserIds, setAllUserIds] = useState(defaultUsersState);
  const [filters, setFilters] = useState({name: '', male: false, female: false});

  useEffect(() => {
    setAllUserIds(filter(filters))
  }, [filters]);

  function handleSelect(id) {
    return function(event) {
      setSelectedUsers((prevState) => ([...prevState, id]));
      setAllUserIds(allUserIds.filter((userId) => userId !== id))
    }
  }

  function handleDeselect(id) {
    return function(event) {
      setSelectedUsers(selectedUserIds.filter((selectedUserId) => selectedUserId !== id));
      setAllUserIds((prevState) => ([...prevState, id]))
    }
  }

  function handleNameFilter(event) {
    return setFilters({...filters, name: event.target.value })
  }

  function handleGenderFilter(event) {
    const { checked, name } = event.target;
    return setFilters({ ...filters, [name]: checked })
  }

  return (
    <div>
      <header>
        <div className="filters">
          <div className="search-field">
            <input onChange={handleNameFilter}
                   placeholder="Name"
                   className="search-field__input"
                   type="text"
                   name="name"
                   id="name" />
            <i className="search-field__icon"/>
          </div>
          <div className="checkbox-field">
            <label htmlFor="male" className="checkbox-field__label">
              <input onChange={handleGenderFilter}
                     className="checkbox-field__input"
                     type="checkbox"
                     name="male"
                     id="male"/>
              Male
            </label>
          </div>
          <div className="checkbox-field">
            <label htmlFor="female" className="checkbox-field__label">
              <input onChange={handleGenderFilter}
                     className="checkbox-field__input"
                     type="checkbox"
                     name="female"
                     id="female"/>
              Female
            </label>
          </div>
        </div>
      </header>

      <main className="main">
        <section className="section-users">
          <h1 className="section-users__title">Selected</h1>
          <ul className="section-users__users-block">
            {selectedUserIds.map((id) => (
              <li key={id} onClick={handleDeselect(id)}>
                <figure className="user-block">
                  <img className="user-block__avatar" alt="Selected User Avatar" src={getUserById(id).avatar} />
                  <figcaption className="user-block__caption">
                    <span className="user-block__name">{getUserById(id).name}</span>
                  </figcaption>
                </figure>
              </li>
            ))}
          </ul>
        </section>

        <section className="section-users">
          <h1 className="section-users__title">All</h1>
          <ul className="section-users__users-block">
            {allUserIds.map((id) => (
              <li key={id} onClick={handleSelect(id)}>
                <figure className="user-block">
                  <img className="user-block__avatar" alt="User Avatar" src={getUserById(id).avatar} />
                  <figcaption className="user-block__caption">
                    <span className="user-block__name">{getUserById(id).name}</span>
                  </figcaption>
                </figure>
              </li>
            ))}
          </ul>
        </section>
      </main>
    </div>
  );
}
